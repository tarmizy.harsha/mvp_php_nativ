Buat sebuah folder includes
 kemudian di dalam folder includes tambahkan sebuah file berikut (config.php)
 
 isi file config.php
 <?php
ob_start();
session_start();

//set timezone
//date_default_timezone_set('Europe/London');

//database credentials
define('DBHOST','ip database');
define('DBUSER','user database');
define('DBPASS','password database');
define('DBNAME','database nama');

//application address
define('DIR','http://localhost');
define('SITEEMAIL','tarmizy13@gmail.com');

try {

        //create PDO connection
        $db = new PDO("mysql:host=".DBHOST.";charset=utf8mb4;dbname=".DBNAME, DBUSER, DBPASS);
    //$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);//Suggested to uncomment on production websites
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);//Suggested to comment on production websites
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

} catch(PDOException $e) {
        //show error
    echo '<p class="bg-danger">'.$e->getMessage().'</p>';
    exit;
}

//include the user class, pass in the database connection
include('classes/user.php');
//include('classes/phpmailer/mail.php');
$user = new User($db);
?>





Jika menggunakan Postgres maka referensinya dapat dilihat di sini
https://www.php.net/manual/en/ref.pdo-pgsql.connection.php